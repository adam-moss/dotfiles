#!/usr/bin/env bash

eval "DEBUG_DOTFILES='$DEBUG_DOTFILES' $DOTFILES_PATH/helpers/debug-dotfiles"

if [ $# -ne 1 ] && [ $# -ne 2 ]; then
  "$DOTFILES_PATH"/helpers/log-to-console "ERROR" "You must provide an identity and optionally a language"

  exit 1
fi

git_host_user="@adam-moss"
git_group="adam-moss"
copyright_holder="Adam Moss"

git init --quiet

if ! add-git-config-local "$1"; then
  exit 1
fi

case "$1" in
  dwp-github)
    git_group="dwp"
    copyright_holder="Crown Copyright (Department for Work and Pensions)"
    ;;

  dwp-gitlab)
    git_group="dwp/@group_path"
    copyright_holder="Crown Copyright (Department for Work and Pensions)"
    ;;

  personal-github | \
    personal-gitlab) ;;

  *)
    "$DOTFILES_PATH"/helpers/log-to-console "ERROR" "Unknown git identity" "ERROR" "Unknown git identity"

    exit 2
    ;;
esac

git commit --allow-empty --message 'chore(repo): initial commit' --quiet --signoff

add-editor-config
add-git-attributes
add-git-ignore

add-commitlint-config "$1"
add-pre-commit-config "$1"

# add-markdown-any-decision-record "$git_host_user"

# pre-commit runs markdownlint-cli2
rm markdownlint-cli2-codequality.json

git checkout -b "feat/$(basename "$(pwd)")" --quiet

add-license "$copyright_holder"
add-citation "${1#*-}" "$git_group"
add-code-owner "${1#*-}" "$git_host_user"
add-gitlab-ci "${1#*-}"
add-read-me "${1#*-}" "$git_group"

set +u

if [ -n "$2" ]; then
  case "$2" in
    python)
      add-poetry-config-local
      add-poetry
      add-python-localisation "$copyright_holder" "${1#*-}" "$git_group"

      ;;
  esac
fi
