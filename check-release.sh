#!/usr/bin/env sh

RELEASE_NOTES="$(npx commit-and-tag-version --dry-run | awk 'BEGIN { flag=0 } /^---$/ { if (flag == 0) { flag=1 } else { flag=2 }; next } flag == 1')"

# Don't release if there are no changes
if [ "$(echo "$RELEASE_NOTES" | wc -l)" -eq 1 ]; then
  printf "This release would have no release notes\n\n"
  printf "*** ABORTING RELEASE PROCESS ***"

  exit 1
fi
