#!/usr/bin/env zsh
# shellcheck shell=bash

if [ "$(uname)" = "Darwin" ]; then
  export SHELL_SESSIONS_DISABLE=1
fi
