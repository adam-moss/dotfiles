#!/usr/bin/env bash

if [ "$(uname)" = "Darwin" ]; then
  if [ ! -f "$HOME/.bash_sessions_disable" ]; then
    touch "$HOME/.bash_sessions_disable"
  fi
fi
