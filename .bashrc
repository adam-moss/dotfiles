#!/usr/bin/env bash

export SCRIPT_LOG_LEVEL="INFO"

shopt -s checkhash
shopt -s dotglob
shopt -s extglob
shopt -s histappend
shopt -s nullglob

export HISTCONTROL="ignorespace:ignoredups:erasedups"
export HISTIGNORE="brew*:cat*:cd*:exit:git*:gpg*:ls*:nmap*:ssh*"
export HISTSIZE=10000

DOTFILES_PATH=$(dirname "$HOME/$(readlink "${BASH_SOURCE[0]}")")/src/dotfiles

export DOTFILES_PATH

for source_file in "$DOTFILES_PATH"/{10-variables,20-aliases,30-functions,40-hooks,50-completions,60-configs}/*; do
  # shellcheck source=/dev/null
  source "$source_file"
done

PATH="$DOTFILES_PATH/:$PATH"

unset source_file
