# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## 1.0.0 (2023-04-06)

### Features

- dotfiles ([4596cce](https://gitlab.com/adam-moss/dotfiles/commit/4596cce646d2a5f44bc21aeb965c5a14834966f6))
