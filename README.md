# Dotfiles

These are my dotfiles. There are many like them, but these files are mine.

My dotfiles are my allies. They enhance my productivity. I must optimise them as I must optimise my life.

Without me, my dotfiles are useless. Without my dotfiles, I am useless. I must maintain my dotfiles true. I must code better than my bugs which are trying to delay me. I must test my code automatically and rigourously. I will...

My dotfiles and I know that what counts in programming is not the amount of code we write, the beauty of the syntax, nor the loudness of our keyboard. We know it is the quality of our code that counts. We will streamline our workflow...

My dotfiles are human, even as I am human, because they are my ally. Thus, I will learn them as a partner. I will learn their weaknesses, their strengths, their aliases, their functions, their commands, and their tests. I will keep my dotfiles updated and shared, as I myself learn and adapt. We will work in harmony with each other. We will...

Before the compiler, I swear this creed. My dotfiles and I are the defenders of my code. We are the conquerors of my bugs. We are the saviours of my productivity.

So be it, until victory is code and there are no bugs, but success through testing!

## Using Dotfiles

### Clone

1. Open your preferred terminal application.
2. Navigate to the directory where you want to clone the repo.
3. Type `git clone https://gitlab.com/adam-moss/dotfiles.git` and press Enter.
4. Wait for the clone to complete.

### Symlink

1. Navigate to your `$HOME` directory.
2. Type `ln -s <path to dotfiles clone> .zshrc` [^1] and press Enter.
3. Verify that the symlink has been created successfully.

[^1]: alternatively, `.bashrc`.

### Activate

1. Save any work that you need to save.
2. Close the terminal application.
3. Open the terminal application again.

That's it! If you follow these steps correctly, you should be able to clone the repo, symlink to it, and activate it without any issues.
