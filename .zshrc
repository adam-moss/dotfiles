#!/usr/bin/env zsh
# shellcheck shell=bash

export SCRIPT_LOG_LEVEL="INFO"

setopt APPEND_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_FCNTL_LOCK
setopt HIST_FIND_NO_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_NO_FUNCTIONS
setopt HIST_NO_STORE
setopt HIST_REDUCE_BLANKS
setopt HIST_SAVE_BY_COPY
setopt HIST_SAVE_NO_DUPS
setopt INC_APPEND_HISTORY
setopt GLOB_COMPLETE
setopt MAIL_WARNING
setopt NOTIFY
setopt SHARE_HISTORY
setopt WARN_NESTED_VAR

unsetopt LIST_BEEP

export HISTORY_IGNORE="brew*|cat*|cd*|exit|git*|gpg*|ls*|nmap*|ssh*"
export HISTSIZE=10000

autoload -Uz add-zsh-hook compinit

compinit

# ${${(%):-%x}:A:h} is a zsh construct
# - shellcheck doesn't support zsh
# - prettier-ignore doesn't work in prettier-plugin-sh
# - shellcheck disable doesn't work in prettier-plugin-sh
# shellcheck disable=SC2296,SC2298
DOTFILES_PATH="${${(%):-%x}:A:h}"/src/dotfiles

export DOTFILES_PATH

for source_file in "$DOTFILES_PATH"/{10-variables,20-aliases,30-functions,40-hooks,50-completions,60-configs}/*; do
  # shellcheck source=/dev/null
  source "$source_file"
done

compinit

PATH="$DOTFILES_PATH:$PATH"

unset source_file
